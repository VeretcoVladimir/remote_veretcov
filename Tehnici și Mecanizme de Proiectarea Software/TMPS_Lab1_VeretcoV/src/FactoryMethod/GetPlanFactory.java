package FactoryMethod;

class GetPlanFactory{

    public Plan getPlan(String planType){
        if(planType == null){
            return null;
        }
        if(planType.equalsIgnoreCase("d")) {
            return new DomesticPlan();
        }
        else if(planType.equalsIgnoreCase("c")){
            return new CommercialPlan();
        }
        else if(planType.equalsIgnoreCase("i")) {
            return new InstitutionalPlan();
        }
        return null;
    }
}
