import java.util.concurrent.TimeUnit;

public class MultiThreading {
    public static void firstGroup(){
        Thread one = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Thread one");
            }
        });

        one.start();

        try {
            one.join();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void secondGroup(){
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Thread two = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Thread two");
            }
        });

        Thread three = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Thread three");
            }
        });

        two.start();
        three.start();

        try {
            two.join();
            three.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void thirdGroup(){
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Thread four = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Thread four");
            }
        });

        Thread five = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Thread five");
            }
        });

        four.start();
        five.start();

        try {
            four.join();
            five.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void fourthGroup(){
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Thread six = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Thread six");
            }
        });

        six.start();

        try {
            six.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}

